package es.adrianmmudarra.expoacdat.UI;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import es.adrianmmudarra.expoacdat.Memory.Memoria;
import es.adrianmmudarra.expoacdat.Model.Resultado;
import es.adrianmmudarra.expoacdat.Network.RestClient;
import es.adrianmmudarra.expoacdat.R;
import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    EditText ed_url_imagen;
    EditText ed_url_frase;
    ImageView imageView;
    Button btn_descarga;
    TextView tv_frases;
    String[] frases;
    String[] imagenes;

    static final int REQUEST_CONNECT = 1;
    static int segundos;
    static final String CODIGO = "UTF-8";
    Memoria mimemoria = new Memoria(this);
    CountDownTimer timer = null;
    final static String WEB = "http://adrianm.alumno.mobi/upload.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializacion();
    }

    private void inicializacion() {
        ed_url_frase = findViewById(R.id.ed_URL_frases);
        tv_frases = findViewById(R.id.tv_frases);
        ed_url_imagen = findViewById(R.id.ed_URL_imagenes);
        imageView = findViewById(R.id.iv_imagenes);
        btn_descarga = findViewById(R.id.btn_Descargar);
        btn_descarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
                comprobacion();
            }
        });
    }
    private void reset(){
        if(mimemoria.disponibleEscritura()){
            mimemoria.escribirExterna("errores.txt","",false,CODIGO);
        }
        if (timer!= null){
            timer.cancel();
            timer = null;
        }
    }

    private void comprobacion() {
        if(comprobarPermisos()){
            if(comprobarFicheroIntervalo()){
                descargaFrases();
                descargaImagenes();
            }
        }
    }

    private boolean comprobarPermisos() {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        boolean concedido = false;
        if (ActivityCompat.checkSelfPermission(this, permiso) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permiso}, REQUEST_CONNECT);
        } else {
            concedido = true;
        }
        return concedido;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (requestCode == REQUEST_CONNECT)
            if (ActivityCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED){
                if(comprobarFicheroIntervalo()){
                    descargaFrases();
                    descargaImagenes();
                }
            }
            else
                Toast.makeText(MainActivity.this,"No se ha concedido el permiso",Toast.LENGTH_SHORT);
        subirErrores("No se ha concedido el permiso");
    }

    private boolean comprobarFicheroIntervalo() {
        Resultado r = mimemoria.leerRaw("intervalo.txt");
        if (r.getCodigo()){
            try {
                segundos = Integer.parseInt(r.getContenido());
            }catch (Exception e){
                subirErrores("El fichero intervalo no tiene el formato correcto");
            }

        } else {
            subirErrores("Error al buscar en RAW");
            return false;
        }
        return true;
    }

    private void descargaFrases() {
        final ProgressDialog progreso = new ProgressDialog(this);
        final File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        RestClient.get(ed_url_frase.getText().toString(), new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(true);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                subirErrores("Error al descargar el fichero de frases");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                lectura();
                Toast.makeText(MainActivity.this,"200 Ok", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void lectura() {
        Resultado r;
        String texto;
        if (mimemoria.disponibleLectura()){
            r = mimemoria.leerExterna("frases.txt",CODIGO);
            texto = r.getContenido();
            frases = texto.split("\\r?\\n");
            tv_frases.setText(frases[(int)(Math.random() * (frases.length - 0))]);
        }
    }

    private void descargaImagenes() {
        final ProgressDialog progreso = new ProgressDialog(this);
        final File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        RestClient.get(ed_url_imagen.getText().toString(), new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(true);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                subirErrores("Error al descargar el fichero con imagenes");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                lecturaImagenes();
                Toast.makeText(MainActivity.this,"200 Ok", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void lecturaImagenes() {
        Resultado r;
        final String texto;
        if (mimemoria.disponibleLectura()){
            r = mimemoria.leerExterna("imagenes.txt",CODIGO);
            texto = r.getContenido();
            imagenes = texto.split("\\r?\\n");
            OkHttpClient client = new OkHttpClient();
            Picasso picasso = new Picasso.Builder(this).downloader(new OkHttp3Downloader(client)).build();
            picasso.get().load(imagenes[(int)(Math.random() * (imagenes.length - 0))]).placeholder(R.drawable.placeholder).error(R.drawable.placeholder_error).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    Toast.makeText(MainActivity.this, "Descarga OK", Toast.LENGTH_SHORT).show();
                    temporizador();
                }

                @Override
                public void onError(Exception e) {
                    subirErrores("Error al descargar una imagen");
                    temporizador();
                }
            });
        }
    }

    void temporizador() {
        timer = new CountDownTimer((long)segundos*1000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                tv_frases.setText(frases[(int)(Math.random() * (frases.length - 0))]);

                OkHttpClient client = new OkHttpClient();
                Picasso picasso = new Picasso.Builder(MainActivity.this).downloader(new OkHttp3Downloader(client)).build();
                picasso.get().load(imagenes[(int)(Math.random() * (imagenes.length - 0))]).placeholder(R.drawable.placeholder).error(R.drawable.placeholder_error).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(MainActivity.this, "Descarga OK", Toast.LENGTH_SHORT).show();
                        temporizador();
                    }

                    @Override
                    public void onError(Exception e) {
                        subirErrores("Error al descargar una imagen");
                        temporizador();
                    }
                });
            }
        };
        timer.start();
    }

    private void subirErrores(String error){
        if(mimemoria.disponibleEscritura()){
            mimemoria.escribirExterna("errores.txt",Calendar.getInstance().getTime().toString()+" - "+error+System.lineSeparator(),true,CODIGO);
        }

        final ProgressDialog progreso = new ProgressDialog(this);
        File myFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "errores.txt");
        RequestParams params = new RequestParams();
        Boolean existe = true;
        try {
            params.put("fileToUpload",myFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT);
            existe = false;
        }

        if (existe){
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Conectando . . .");
                    progreso.setCancelable(true);
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });
                    progreso.show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progreso.dismiss();
                    Toast.makeText(MainActivity.this,responseString,Toast.LENGTH_SHORT);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    progreso.dismiss();
                    Toast.makeText(MainActivity.this,responseString,Toast.LENGTH_SHORT);
                }
            });
        }
    }
}
