# Trabajo ACDAT

Este repositorio es un trabajo del módulo "Acceso a datos" el cual consiste en hacer uso de lo aprendido en este modulo con respecto a ficheros y redes.


## Planteamiento

Lo que he intentado hacer con este trabajo es poder dividirlo en métodos.
> Divide y vencerás (Profesores I.E.S Portada Alta)


## Configuración necesaria

Lo único que necesita para que funcione es un fichero **intervalo.txt** ubicado en la carpeta raw.

## Mejoras

El trabajo necesita una buena mano de refactorización y arreglar un pequeño problema que es cuando lleva mucho tiempo ejecutándose y el intervalo es pequeño.
